let pkgs = import <nixpkgs> {};

# Inspired from https://nixos.wiki/wiki/Android#Building_Android_on_NixOS
fhs = pkgs.buildFHSUserEnv {
	name = "android-env";
	targetPkgs = pkgs: with pkgs; [
		SDL
		androidenv.androidPkgs_9_0.platform-tools
		bc
		binutils
		bison
		ccache
		curl
		flex
		fontconfig
		freetype
		gcc
		git
		gitRepo
		gnumake
		gnupg
		gperf
		imagemagick
		jdk
		kmod
		libressl
		libxml2
		lz4
		lzop
		m4
		nettools
		openssl
		openssl.dev
		perl
		pngcrush
		procps
		python2
		rsync
		schedtool
		squashfsTools
		unzip
		utillinux
		wxGTK30
		xml2
		zip
	];
	multiPkgs = pkgs: with pkgs; [
		libcxx
		ncurses5
		openssl
		readline
		zlib
	];
	runScript = "bash";
	profile = ''
		export USE_CCACHE=1
		export ANDROID_JAVA_HOME=${pkgs.jdk.home}
		# Building involves a phase of unzipping large files into a temporary directory
		export TMPDIR=/tmp
		# Only needed if you want to add prebuilt apks https://gist.github.com/cheriimoya/6a4ffdd10ccf66961a748f320e8a9d41#file-build_lineage_for_davinci-md
		#export CUSTOM_PACKAGES="GmsCore GsfProxy FakeStore MozillaNlpBackend \
	    #				NominatimNlpBackend com.google.android.maps.jar FDroid \
	    #    			FDroidPrivilegedExtension "
	'';
};

in pkgs.stdenv.mkDerivation {
	name = "android-env-shell";
	nativeBuildInputs = [ fhs ];
	shellHook = "exec android-env";
}
